
import sublime
import os
import subprocess
import threading
from functools import partial
from sublime_plugin import WindowCommand


class StatusHandler:

    TIME = 50  # 50 ms delay
    OPTS = '-\\|/'

    def __init__(self, thread, msg):
        self.counter = 0
        # self.direction = 1
        self.msg = msg
        self.thread = thread

    def progress(self):
        if not self.thread.is_alive():
            sublime.status_message('')
            return

        status = '[{0}] {1}'.format(self.OPTS[self.counter], self.msg)
        self.counter = (self.counter + 1) % len(self.OPTS)
        sublime.status_message(status)
        sublime.set_timeout(self.progress, self.TIME)

    def start(self):
        self.thread.start()
        sublime.set_timeout(self.progress, 0)


class AsyncCmd:

    status_fmt = ''

    def async_cmd(self, **args):
        raise NotImplemented("async_cmd must be overridden in a subclass")

    def create_thread(self, **args):
        return threading.Thread(target=partial(self.async_cmd,**args))

    def run_async(self, **args):
        thread = self.create_thread(**args)
        thread.start()
        return thread

    def run_async_with_status(self, msg='', **args):
        thread = self.create_thread(**args)
        runner = StatusHandler(thread, msg)
        runner.start()
        return runner

    def run(self, **args):
        self.run_command(**args)

    def run_command(self, **args):
        if self.status_fmt:
            self.run_async_with_status(
                msg=self.status_fmt.format(args),
                on_data=self.on_data,
                on_complete=self.on_complete,
                on_error=self.on_error,
                on_exception=self.on_exception,
                **args
            )
        else:
            self.run_async(
                on_data=self.on_data,
                on_complete=self.on_complete,
                on_error=self.on_error,
                on_exception=self.on_exception,
                **args
            )

    def handle(self, event, *args):
        pass

    def on_data(self, *args):
        self.handle('data', *args)

    def on_complete(self, *args):
        self.handle('complete', *args)

    def on_error(self, *args):
        self.handle('error', *args)

    def on_exception(self, *args):
        self.handle('exception', *args)
