import sublime
import sublime_plugin
import os
import json
import requests
import subprocess
from pathlib import Path
from .async_helper import AsyncCmd

gh_gi_url_fmt = 'https://raw.githubusercontent.com/github/gitignore/master/{}.gitignore'

default_gitignores = [
    'Global/Linux',
    'Global/Windows',
    'Global/macOS',
    'Global/SublimeText'
]


def get_executable_path():
    executable_path = sublime.executable_path()
    if sublime.platform() == 'osx':
        app_path = executable_path[:executable_path.rfind(".app/")+5]
        executable_path = app_path+"Contents/SharedSupport/bin/subl"
    return executable_path


class SimpleTextInputHandler(sublime_plugin.TextInputHandler):

    def __init__(self, name, placeholder='', initial_text='', next_input=None):
        super(SimpleTextInputHandler, self).__init__()
        self._name = name
        self._placeholder = placeholder
        self._initial_text = initial_text
        self._next_input = next_input

    def name(self):
        return self._name

    def placeholder(self):
        return self._placeholder

    def initial_text(self):
        return self._initial_text

    def next_input(self, args):
        return self._next_input


class FileExistsContextListener(sublime_plugin.EventListener):

    """
    "context": [{"key": "exists"}]
    Only runs when a Pipfile is present in ${folder}.
    """

    def on_query_context(self, view, key, operator, operand, match_all):
        if key == "exists" and isinstance(operand, str):
            winvars = view.window().extract_variables()
            path = Path(sublime.expand_variables(operand, winvars))
            print(dir(path))
            print(path)
            if operator == sublime.OP_EQUAL and path.exists():
                return True
            elif operator == sublime.OP_NOT_EQUAL and not path.exists():
                return True


class NewProject(AsyncCmd, sublime_plugin.WindowCommand):
    def async_cmd(self, name, location=None, sublime_location='.', repo=None, callback=None, **kwargs):
        pd = self.location(location) / name
        sd = Path(os.path.expanduser(sublime_location))
        if not sd.is_absolute():
            sd = pd / sd
        sf = sd / '{}.sublime-project'.format(name)
        print(str(pd))
        print(str(sd))
        print(str(sf))

        if not pd.exists():
            pd.mkdir(parents=True)
        #     raise FileExistsError(pd)

        if not sd.exists():
            sd.mkdir(parents=True)

        self.setup_repo(pd, repo)

        with open(str(sf), 'w') as fp:
            json.dump({
                'folders': [{'path': Path(os.path.relpath(str(pd), str(sd))).as_posix()}],
                'settings': kwargs.get('settings', {})
            }, fp, indent=4)

        gif = pd / '.gitignore'

        if not gif.exists():
            text = ''
            for gi in default_gitignores + kwargs.get('gitignores', []):
                text += '# {0}\n'.format(gi)
                text += requests.get(gh_gi_url_fmt.format(gi)).text
                text += '\n'
            with open(str(gif), 'w') as fp:
                fp.write(text)

        self.open_project(sf)

    def runcmd(self, cmd, cwd=None, env=None, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL):
        return subprocess.Popen(cmd, cwd=cwd, env=env, stdout=stdout, stderr=stderr).wait()

    def subl(self, *args, **kwargs):
        return self.runcmd((get_executable_path(),)+args, **kwargs)

    def location(self, location=None):
        if location is None:
            location = self.window.settings().get('project_home', os.getenv('PROJECT_HOME', '~/Projects'))
        return Path(os.path.expanduser(location))

    def open_project(self, filename):
        self.subl('--project', str(filename))

    def setup_repo(self, path, repo=None):
        self.runcmd(['git', 'init'], cwd=str(path))
        if repo:
            self.runcmd(['git', 'remote', 'add', 'origin', repo])
            self.runcmd(['git', 'pull', 'origin', 'master'])

    def input(self, args):
        repo = SimpleTextInputHandler('repo', 'Git Repo') if 'repo' not in args else None
        if 'name' not in args:
            return SimpleTextInputHandler('name', 'Project Name', '', repo)
        return repo


class PipenvProject(NewProject):

    status_fmt = 'Creating project {0[name]}'

    def async_cmd(self, name, location=None, **kwargs):
        pd = self.location(location) / name

        if not pd.exists():
            pd.mkdir(parents=True)
        #     raise FileExistsError(pd)

        # self.runcmd(['pipenv', '--three'], str(pd), {**os.environ, 'PIPENV_VENV_IN_PROJECT': 'Y'})
        env = os.environ.copy()
        env['PIPENV_VENV_IN_PROJECT'] = 'Y'
        self.runcmd(['pipenv', '--three'], str(pd), env)
        kwargs.setdefault('gitignores', []).append('Python')
        kwargs.setdefault('settings', {})['python_interpreter'] = '.venv/bin/python'
        super().async_cmd(name, location, **kwargs)
        # super().async_cmd(name, location, **{
        #     **kwargs,
        #     'gitignores': [*kwargs.get('gitignores', []), 'Python'],
        #     'settings': {**kwargs.get('settings', {}), 'python_interpreter': '.venv/bin/python'},
        # })
